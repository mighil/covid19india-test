import React, {useState, useEffect} from 'react';

function Links(props) {
  return (
    <div className="Links">

      <div className="link fadeInUp" style={{animationDelay: '0.2s'}}>
      <h3>GoK Direct App</h3>
<p>Get official information regarding the COVID-19 outbreak. Available on <a href="https://play.google.com/store/apps/details?id=com.qkopy.prdkerala">Playstore</a> and <a href="https://apps.apple.com/in/app/gok-direct/id1502436125">Appstore</a>.</p>
    <h3>Coping During COVID-19</h3>
      <p>  How are you coping with isolation and social distancing? Widespread media reporting of the coronavirus pandemic, changing daily circumstances and uncertainty about the future can give rise to heightened individual and community anxiety.</p>
<p> <h3>Looking after your mental health in home isolation</h3>
 <ul>
  <li>Remind yourself that this is a temporary period of isolation</li>
  <li>Remember that your effort is helping others in the community</li>
  <li>Stay connected with friends, family, and colleagues via email, social media, or phone</li>
  <li>Engage in healthy activities that you enjoy and find relaxing</li>
  <li>Keep regular sleep routines and eat healthy foods</li>
  <li>Try to maintain physical activity</li>
  <li>For those working from home, try to maintain a healthy balance by allocating specific work hours, and taking regular breaks</li>
  <li>Avoid checking news and social media regularly if you find it distressing</li>
</ul> </p>
<h3>COVID-19 Related Tools Built by Malayali Makers</h3>
        <p><ul>
  <li><a href="https://medium.com/@lakshmisunil12/%E0%B4%95%E0%B5%8B%E0%B4%B5%E0%B4%BF%E0%B4%A1%E0%B5%8D-19-%E0%B4%92%E0%B4%B0%E0%B5%81-%E0%B4%A1%E0%B4%BE%E0%B4%B1%E0%B5%8D%E0%B4%B1%E0%B4%BE%E0%B4%B8%E0%B4%AF%E0%B4%A8%E0%B5%8D%E0%B4%B1%E0%B4%BF%E0%B4%B8%E0%B5%8D%E0%B4%B1%E0%B5%8D%E0%B4%B1%E0%B4%BF%E0%B4%A8%E0%B5%8D%E0%B4%B1%E0%B5%86-%E0%B4%B5%E0%B5%80%E0%B4%95%E0%B5%8D%E0%B4%B7%E0%B4%A3%E0%B4%95%E0%B5%8B%E0%B4%A3%E0%B4%BF%E0%B4%B2%E0%B5%8D-41bf10cfbf6e">കോവിഡ്-19 — ഒരു ഡാറ്റാസയന്റിസ്റ്റിന്റെ വീക്ഷണകോണില്‍</a><em>, Lakshmi Sunil</em></li>
  <li><a href="https://drive.google.com/file/d/1mIsxcbIZG_nZCISpqkan60yRVO1dVraW/view">PDF guide to COVID-19</a><em>, Anjana Prabhu-Paseband</em></li>
  <li><a href="https://www.coronasafe.in/">CoronaSafe Network,</a> <em>@CoronaSafe</em></li>
  <li><a href="https://github.com/ashishkhuraishy/CovidUpdates/raw/master/output/Covid%20Updates.apk">Covid Updates Android APP</a><em>, Ashish</em></li>
</ul></p>
<h3>Free ebooks</h3>
        <p><ul>
  <li><a href="https://ebooks.dcbooks.com/category/bookshelf/free-books">1 free ebook each day</a> <em>, DC Books</em></li>
  <li><a href="https://b-ok.cc/">ebook Library</a><em>, ZLibrary</em></li>
</ul></p>
<p><h3>Films to Watch</h3>
<ul>
  <li><a href="https://www.bbc.com/culture/story/20200319-covid-19-comforting-films-to-watch-in-isolation">The most comforting films for challenging times</a></li>
  <li><a href="https://www.nme.com/blogs/the-movies-blog/coronavirus-wholesome-movies-to-watch-while-self-isolating-and-where-to-stream-them-2626533">Wholesome movies to watch while self-isolating</a></li>
  <li><a href="https://fashionjournal.com.au/film/a-very-comprehensive-list-of-films-to-watch-while-youre-self-isolating/">List of films to watch while you’re self-isolating</a></li>
  <li><a href="https://github.com/k4m4/movies-for-hackers">Top 5 feminist films on Netflix</a></li>
  <li><a href="https://www.instagram.com/too_much_equal/">Movies for hackers</a></li>
  <li><a href="https://mighil.com/100-greatest-movies-of-all-time/">100 greatest movies, list by Mighil</a></li>
  <li><a href="https://drive.google.com/drive/u/0/folders/1aZmFnNswNHbKwSOclbNB189zfCa9l7Yb">Collection of films released between 1800 ~ 2020</a></li>
</ul> </p>
<h3>Malayalam Audio Books</h3>
<p>വായിക്കാൻ ആഗ്രഹിച്ച 50 എഴുത്തുകാരുടെ കഥകൾ ഇനി വായിച്ചു കേൾക്കാം തിരക്കു പിടിച്ച ഈ ലോകത്ത്‌ വായിക്കാൻ ആഗ്രഹിച്ച കഥകൾ വായിച്ചു കേൾക്കാൻ, വായിച്ചു പതിഞ്ഞ കഥകൾ ഒരിക്കൽ കൂടി വായിച്ചു കേൾക്കാൻ ഡെയ്‌ലി ന്യൂസ്‌ വായനാലോകം. Sucbscribe to their <a href="https://www.youtube.com/channel/UCtLKJYfFPFTScpMYDF9IMVQ/featured">YouTube channel.</a></p>
    <p>  <h3>Other Links</h3>
        <ul>
  <li><a href="https://www.mohfw.gov.in/">Ministry of Health and Family Welfare, Gov. of India</a></li>
  <li><a href="https://www.who.int/emergencies/diseases/novel-coronavirus-2019">WHO : COVID-19 Home Page</a></li>
  <li><a href="https://www.cdc.gov/coronavirus/2019-ncov/faq.html">CDC</a></li>
  <li><a href="https://coronavirus.thebaselab.com/">COVID-19 Global Tracker</a></li>
</ul> </p>
      </div>
    
    </div>
  );
}

export default Links;
